CXX=g++ -std=gnu++11

all: problem_materials

problem_materials: problem_statement data solutions

data:
	curl -o temp.zip "http://acm-ecna.ysu.edu/PastResults/2016/$(PROBLEM).zip"
	unzip temp.zip
	rm temp.zip
	#Cleanup file structure
	mv $(PROBLEM)/* .
	rm -r $(PROBLEM) input_format_validators
	mv data/sample/* data
	mv data/secret/* data
	rm -r data/sample data/secret

problem_statement: data
	mv problem.yaml problem_statement

solutions: data
	mkdir solutions
	mv submissions/accepted/* solutions
	rm -r submissions

answer: answer.cpp
	$(CXX) answer.cpp -o answer

check: problem_materials answer
	@for PROBNUM in $$(ls data | cut -f1 -d. | uniq) ; do \
		./answer < data/$$PROBNUM.in > temp.ans ; \
	  if ! diff temp.ans data/$$PROBNUM.ans > /dev/null ; then \
		  echo "Failed problem $$PROBNUM" ; \
	  fi \
	done
	@rm temp.ans

clean:
	rm -rf data solutions problem_statement answer
