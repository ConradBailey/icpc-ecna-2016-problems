all:
	curl -o problem_set.pdf "http://acm-ecna.ysu.edu/PastResults/2016/prac2016.pdf"

	for i in $$(ls -d */) ; do \
	  $(MAKE) -C "$$i" all ; \
	done

clean:
	rm problem_set.pdf

	for i in $$(ls -d */) ; do \
	  $(MAKE) -C "$$i" clean ; \
	done
