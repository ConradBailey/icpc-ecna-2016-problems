// This is a pretty brute force approach.
//
// First, you ingest the grid as given and store it in a matrix, or Grid
// (vector<vector<char>>)
//
// Next you replace all of the X's in that grid with the correct
// perimeter letter, i.e. the perimeter of the 'A' section should be
// comprised of A's, etc. It's important to leave the intersections of
// the perimeters as X's though. This is done by recursively infecting
// neighbors, starting from the given 'A' and 'B' cells.
//
// Now make a new, empty grid with dimensions equal to the original,
// where all cells inside the A section will be recorded. To find
// those cells, first find a single cell that lies in the section
// (this is complicated. See Vin_Diagram.is_for_sure_inside()), and
// recursively explore its neighbors until the perimeter is
// encountered. Repeat for the B section
//
// Finally simply count the cells exclusive to and shared by the two
// grids you built in the previous step.

#include <iostream>
#include <vector>

using namespace std;


// Just a wrapper class for vector<vector<char>> with infinite read access,
// but bounded write access
class Grid : public vector<vector<char>> {
public:
	Grid(int r, int c) :
		vector<vector<char>>(r, vector<char>(c, empty_cell)),
		rows(r), cols(c)
	{}

	// Assume empty_cell for infinite read access
	char at(int r, int c) {
		if (r >= rows ||
				c >= cols ||
				r < 0 ||
				c < 0)
			return empty_cell;

		return (*this)[r][c];
	}

	void insert(int r, int c, char x) {
		(*this)[r][c] = x;
	}

	static const char empty_cell = '.';
	int rows, cols;
};

// This line is needed due to a standards quirk. See
// https://stackoverflow.com/questions/4891067/weird-undefined-symbols-of-static-constants-inside-a-struct-class
const char Grid::empty_cell;





class Vin_Diagram {
public:

	// ab_grid holds the grid as given. The X's will be replaced with the
	//   letter corresponding to the loop to which they belong, except
	//   at intersections where the X's will remain.
	// a_grid will have 'a' in cells belonging to loop A
	// b_grid will have 'b' in cells belonging to loop B
	// Both a_grid and b_grid will have '0' in cells belonging to the other
	//   loop's border
	Vin_Diagram(int r, int c) :
		rows(r), cols(c),
		a_grid(r,c),
		b_grid(r,c),
		ab_grid(r,c)
	{}


	// Force the borders to become infected, i.e. replace X's with the
	// correct loop designation ('A','B',or 'X' at intersections)
	void identify_perimeters() {
		int ar, ac, br, bc;
		char x;
		// Find the initial 'A' and 'B' coordinates
		for (int r = 0; r < rows; ++r) {
			for (int c = 0; c < cols; ++c) {
				x = ab_grid.at(r,c);
				if (x == 'A') {
					ar = r;
					ac = c;
				}
				else if (x == 'B') {
					br = r;
					bc = c;
				}
			}
		}
		identify_perimeter(ar, ac, 'A');
		identify_perimeter(br, bc, 'B');
	}

	void identify_perimeter(int r, int c, char x) {
		identify_perimeter_helper(r, c, r+1, c, x);
		identify_perimeter_helper(r, c, r-1, c, x);
		identify_perimeter_helper(r, c, r, c+1, x);
		identify_perimeter_helper(r, c, r, c-1, x);
	}

	void identify_perimeter_helper(int prevr, int prevc, int r, int c, char x) {
		// Bounds checks
		if ((r < 0 || r == rows) ||
				(c < 0 || c == cols))
			return;

		// Base Case: Don't affect non-X's
		if (ab_grid.at(r, c) != 'X')
			return;

		// Don't spread in all directions at intersection
		if (is_intersection(r,c)) {
			identify_perimeter_helper(r, c,
																r + (r - prevr),
																c + (c - prevc),
																x);
		}
		// Otherwise try to spread
		else {
			ab_grid.insert(r, c, x);
			identify_perimeter(r, c, x);
		}
	}

	bool is_intersection(int r, int c) {
		// Bounds check
		if ((r == 0 || r == rows-1) ||
				(c == 0 || c == cols-1))
			return false;

		// Defined in problem statement
		return (ab_grid.at(r, c) != '.' &&
						ab_grid.at(r-1, c) != '.' &&
						ab_grid.at(r+1, c) != '.' &&
						ab_grid.at(r, c-1) != '.' &&
						ab_grid.at(r, c+1) != '.');
	}

	// Extend a ray to the right of the point. If the number of
	// perimeter intersections is odd, then it is inside.
	//
	// Points colinear with horizontal edges are unreliable. Return
	// false.
	bool is_for_sure_inside(int row, int col, char perim) {
		if (ab_grid.at(row, col) != '.')
			return false;

		bool inside = false ;
		for (int c=col+1; c < cols; ++c) {
			// horizontal edge detection
			if (ab_grid.at(row, c) == perim &&
					ab_grid.at(row, c+1) == perim) {
				return false;
			}

			// Ray intersection with edge
			if (ab_grid.at(row, c) == perim ||
					ab_grid.at(row, c) == 'X') {
				inside = !inside;
			}
		}
		return  inside;
	}

	// Recursively fill a loop
	void fill(char perim, char filler, Grid& g) {
		for (int r=0; r < rows; ++r) {
			for (int c=0; c < cols; ++c) {
				// Find a guaranteed starting point and start filling
				if (is_for_sure_inside(r,c,perim)) {
					fill_helper(r,c,perim,filler,g);
					return;
				}
			}
		}
	}

	void fill_helper(int r, int c, char perim, char filler, Grid& g) {
		// Base cases for perimeters, intersections, cells already filled,
		// and cells taken by the other loop's border, respectively.
		if (ab_grid.at(r,c) == perim ||
				ab_grid.at(r,c) == 'X' ||
				g.at(r,c) == filler ||
				g.at(r,c) == '0')
			return;

		// Replace space with filler and mark cells taken by the other
		// loop's border
		if (ab_grid.at(r,c) == '.') {
			g.insert(r,c,filler);
		}
		else {
			g.insert(r,c,'0');
		}
		// Spread in all directions;
		fill_helper(r+1, c,perim,filler,g);
		fill_helper(r-1, c,perim,filler,g);
		fill_helper(r, c+1,perim,filler,g);
		fill_helper(r, c-1,perim,filler,g);
	}

	void print() {
		string beg = "", end = "\033[49m";
		bool ina, inb;
		for (int r=0; r < rows; ++r) {
			for (int c=0; c < cols; ++c) {
				ina = a_grid.at(r,c) == 'a';
				inb = b_grid.at(r,c) == 'b';

				if (ina && inb) beg = "\033[42m"; // Green
				else if (ina) beg = "\033[43m"; // Yellow
				else if (inb) beg = "\033[44m"; // Blue
				else beg = "";

				cout << beg << ab_grid.at(r,c) << end;
			}
			cout << endl;
		}
	}

	int rows, cols;
	Grid ab_grid;
	Grid a_grid;
	Grid b_grid;
};


int main() {
	int rows, cols;
	cin >> rows >> cols;

	// Ingest Data
	Vin_Diagram vd(rows, cols);
	char x;
	for (int r=0; r < rows; ++r) {
		for (int c=0; c < cols; ++c) {
			cin >> x;
			vd.ab_grid.insert(r, c, x);
		}
	}

	vd.identify_perimeters();
	vd.fill('A','a',vd.a_grid);
	vd.fill('B','b',vd.b_grid);

	//grid.print();

	// Count spaces
	int a = 0, b = 0, ab = 0;
	bool ina, inb;
	for (int r=0; r < rows; ++r) {
		for (int c=0; c < cols; ++c) {
			ina = vd.a_grid.at(r,c) == 'a';
			inb = vd.b_grid.at(r,c) == 'b';

			a += ina && !inb;
			b += inb && !ina;
			ab += ina && inb;
		}
	}

	cout << a << ' ' << b << ' ' << ab << endl;
}
