// This is not the most efficient solution computationally, but
// perhaps requires the least amount of code.
//
// Inputs are guaranteed max length of 100 chars, so we can brute
// force check every macro possible. We just have to be careful with
// our counting.

#include <iostream>
#include <string>
using namespace std;

int main() {
	string input;
	cin >> input;
	int ilen = input.length();

	string macro;

	// Loop through all possible macros
	int min = ilen;
	for (int mlen=2; mlen < ilen; ++mlen) {
		for (int mpos=0; mpos < ilen - mlen; ++mpos) {
			macro = input.substr(mpos, mlen);

			int count = mlen;
			int pos=0;
			// Iterate of the input, looking for instances of the macro
			while (pos < ilen) { // pos == position
				++count;
				// Upon encountering an instance of the macro, skip to its
				// end.
				pos += (input.substr(pos, mlen) == macro) ? mlen : 1;
			}
			//cerr << "  count = " << count << endl;
			if (count < min) min = count;
		}
	}

	cout << min << endl;
}
