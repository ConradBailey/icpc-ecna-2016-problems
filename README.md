* Answers in this repo are somewhat in the spirit of a programming
  competition. For example, there is not much attention given to
  access restriction on classes and naming is not a top priority.

* Run `make all` in a particular problem's directory to download and
  structure inputs, outputs, solutions, and problem statements. This
  can be done in the appropriate directory for a single problem, or at
  the top level for all problems.

* All answers should be contained in an `answer.cpp` file in the
  appropriate directory.

* Run `make check` to check your answer's outputs against accepted
  outputs for every example and tested problem.

## Making a new problem Makefile ##

1. Make a new directory for the problem following the established
	 convention.

2. Copy the Makefile from some other problem to use as a template

3. Go to the [problem set website](http://acm-ecna.ysu.edu/PastResults/2016/problemset.html)
   and find the link for the problem you want to solve.

4. Set the value of PROBLEM to the filename in that link. For example,
   the link for problem A - Dinoscan is
   `http://acm-ecna.ysu.edu/PastResults/2016/dinoscan.zip`
   so for that problem the first line in the makefile would be
   `export PROBLEM = dinoscan`

5. That's it!
